Name:           gtkmm24
Version:        2.24.5
Release:        7
Summary:        C++ interface for GTK2 (a GUI library for X)
License:        LGPLv2+
URL:            http://www.gtkmm.org/
Source0:        http://ftp.gnome.org/pub/GNOME/sources/gtkmm/2.24/gtkmm-%{version}.tar.xz
BuildRequires:  gcc-c++ glibmm24-devel >= 2.24 atkmm-devel >= 2.22 gtk2-devel >= 2.24
BuildRequires:  cairomm-devel >= 1.2.2 pangomm-devel >= 2.26

%description
gtkmm provides a C ++ interface to the GTK + GUI library. gtkmm2 is packed
with GTK + 2.It mainly includes type-safe callbacks, which can be used to
quickly create a complete set of widget classes by inheriting extended
widgets, which can be freely combined.

%package        devel
Summary:        Development files for gtkmm24
Requires:       %{name} = %{version}-%{release}
Requires:       gtk2-devel glibmm24-devel atkmm-devel pangomm-devel cairomm-devel

%description    devel
The gtkmm24-devel package includes header files and libraries necessary
for the gtkmm24 library.

%package        help
Summary:        This package contains help documents
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release} glibmm24-doc
Provides:       gtkmm24-docs
Obsoletes:      gtkmm24-docs

%description    help
Files for help with gtkmm24.

%prep
%autosetup -n gtkmm-%{version} -p1

%build
%configure
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make_build

%install
%make_install
%delete_la

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc COPYING
%{_libdir}/*.so.*

%files devel
%{_includedir}/g*kmm-2.4/
%{_libdir}/*.so
%{_libdir}/g*kmm-2.4/
%{_libdir}/pkgconfig/*.pc

%files help
%doc AUTHORS NEWS README PORTING demos/gtk-demo/
%doc %{_docdir}/gtkmm-2.4
%doc %{_datadir}/devhelp/

%changelog
* Wed Nov 27 2019 gulining<gulining1@huawei.com> - 2.24.5-7
- Pakcage init
